#!/bin/bash

if [[ `uname` != "Darwin" ]]; then
	echo Currently only runs on mac OSX. Sorry!
	exit 1
fi

SRC_LILA=`pwd`
DEST_LILA=~/Documents/Pd/externals
LILA_CONFIG_SRC=LilaConfig-Sample.txt
LILA_APC40EXT_SRC=LilaApc40Ext-Sample.pd
LILA_CONFIG_DEST=LilaConfig.txt
LILA_APC40EXT_DEST=LilaApc40Ext.pd

for i in $LILA_CONFIG_SRC $LILA_APC40EXT_SRC; do
    if [[ ! -f $SRC_LILA/$i ]]; then
	echo Missing file $SRC_LILA/$i
	echo Go to the Lila source directory and retry
	exit 1
    fi
done

if [[ -n "$1" ]]; then
	if [[ ! -d "$1" ]]; then
		echo $1: Not a valid directory
		exit 1
	fi
	DEST_LILA="$1"
fi

echo
read -r -p "Are you sure you want to install Lila config files in $DEST_LILA? [y/N] " response
if [[ $response != "y" ]]; then
	exit 1;
fi

for i in CONFIG APC40EXT; do
    D=`eval echo '$DEST_LILA/$LILA_'$i'_DEST'`
    echo === creating $D ...
    if [[ -f $D ]]; then
	echo $D: already exists, skipping ...
	continue
    fi
    S=`eval echo '$SRC_LILA/$LILA_'$i'_SRC'`
    cp $S $D
done
