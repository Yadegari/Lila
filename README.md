# ***Lila***

**Lila** is a computer music instrument implemented in Pure Data.  The word *Lila* is an old Sanskrit word signifying divine play, the play of destruction and creation, or the play of presence in the moment.  The computer music instrument Lila is built based on simple analog processes (e.g., loop, delay, ring modulation, and feedback) whose parameters are controlled precisely by a performative action. Lila samples and transforms the acoustic material played in real-time
based on the actions of the human playing Lila and plays it back; the acoustic performer can improvise more material on this newly created sound.
This becomes a continual and circular process.
The precise real-time control of the parameters allows the Lila improvisor to participate in both micro and macro level of musical formations.
Thus, the computer not only can act as agent of form in macro structure of time (such as it is in music involving tape music) and lead the acoustic performer,
but also provides a musical context in which a human improvisor, using the computer as an instrument, can accompany and respond to the acoustic material.
In this way, the acoustic performer can have the same form of musical freedom which he or she enjoys in a traditional setting in an augmented expressive language.

Network extensions have been added to Lila so that its performer could control multiple instances of the program over the network,
while Lila compensates for the actions of the performer based on the intrinsic network delays.
I am interested in exploration of the play with space over the network, in the same way that I am able to play with delay in a single location,
to turn the physical distance into an ephemeral yet malleable artistic parameter.

## Installation ##

** Via Git **

git clone --recursive git@gitlab.com:Yadegari/Lila.git

cd Lila

git submodule update --init --recursive

## Theory of Operation ##

**TBD**

## Performing with Lila

In this section the performative interface of Lila is discussed.

### Performing with  Delays
Lila provides 3 separate delay lines each with 3 read heads (instances). The value of each delay can be controlled with either performative actions or through message passing.  By default inputs 1 and 2 are sent to delay line 1; inputs 3 and 4 are sent to delay line 2, and inputs 5 and 6 are sent to delay line 3.

The concept of keyboard control of delays is that a key is pushed to *Mark* the base of a delay line, Then a *Set* key is pushed to set the duration of the delay, which will be the time difference between the *Mark*ing of the delay line and the *Set*ing of it. There are various performatively useful ways to *mark* and *set* the multitude of delay lines.

Six keys are used for each set of controls. One set (keys Q, W, E, A, S, D) is used master set to control all the instances of all the delay lines. One set (Keys R, T, Y, F, G, H) are used to control instances of delay line 1, and another set (keys U, I, O, J, K, L) is used to control the instances of delay line 2. The delay line 3 is only controlled by the master control (because of lack of space on the keyboard),  but it is possible to set its values separately through scripting.
<img src="Documentation/Diagrams/LilaControl.png">

Most users will only need to use the master controls. Key "a" is used to mark the base of all the delay lines. Key 'q' will set the duration of read head 1 for all the delay lines. Key 'w' will set the duration of read head 2 for all the delay lines and letter 'e' will set the duration of the read head 3 for all the delay lines. letter 's' will set the value of the delay lines linearly, with the first ready head duration be according to the moment the letter 's' was pushed, and the delay line duration for read head 2 and 3 be twice and three times of the duration of the delay read head 1 respectively.  Letter 'd' the duration of all the delay lines corresponding to the base of each delay read head.

<img src="Documentation/Diagrams/NonShiftControl.png">

Using shift keys, you can set the base for each instance separately. Letter 'Q' (shift q) sets the base of first instances of all delay lines. 'W' sets the base for the 2nd instances of all delay lines, and 'E' sets the base of the 3rd instances of all delay lines. Lila has a single memory for the delay base locations. 'A', swaps the base of all instances of all delay lines with previously set values Letter 'S' (shift 's') arranges the delays geometrically where delay duration of read head 2 is twice the duration of read head 1, and duration of read head 3 is 4 times the duration of read head 1.
<img src="Documentation/Diagrams/ShiftControl.png">


As mentioed above one can set the base and duration of the instances of each delay line separately as well in case the performer chooses to have different delay values for different inputs. Below diagram shows the non-shifted key layout for delay control:

<img src="Documentation/Diagrams/LilaControlFull.png">

Below diagram shows the shift key layout for delay control:

<img src="Documentation/Diagrams/LilaControlFullShift.png">

##Loops##

NAMES BELOW ARE NO LONGER CORRECT _ WILL BE UPDATED SOON

You can use the page *L_record_loop* in the main Lila page, or one of the midi controllers, which can be configured in the "*L_config* page, to record loops. The two variables *L_quantizeLoop* and *L_quantizeLoopStart* effect the loop recording and playback behavior.

When *L_quantizeLoop* is set the first recorded loop sets the shortest loop duration (in variable *L_loopLength* in number of samples), after that the duration of all recorded loops will be an integer multiple of *LoopLength*. If *L_quantizeLoopStart*, loop recording starts at the shortest loop boundary repeat, and loop play will start at the loop repeat boundary to make sure that all loops share the same common denominator in length and in play back time.

If *L_quantizeLoop* is set but *L_quantizeLoopStart* is not set, the period of loops are regulated as above, however, the recording of loop will happen right at the moment recording is initiated (in interface or by midi instrument) and playback of the respective loop will (re)start once stop recording is requested.


##Recording and Playback of Performances##

Push the Record button, and enter a <filename>. Session recording starts the moment a file is chosen. Click the Start/Stop recording button, to stop recording.The recoding process will create a command file named <filename> and an audio file named <filename>_Lila_audio.wav, which will be a multichannel raw recording of the inputs to Lila (4 Channels in Version 0.7).

To playback a recorded session, load the session by choosing <filename>. After the file is loaded, you can replay the session by pushing the play button. A session can be played back multiple times.

## Programming Lila ##






```
