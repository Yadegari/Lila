/* Copyright (c) 1997-1999 Miller Puckette and others.
* For information on usage and redistribution, and for a DISCLAIMER OF ALL
* WARRANTIES, see the file, "LICENSE.txt," in this distribution.  */

/* sampling */

/* LATER make tabread4 and tabread~ */

#include "m_pd.h"
// #include "string.h"

// void garray_usedindsp(t_garray *x);
#define min(a,b) ((a) < (b) ? (a) : (b))

/* ------------------------- tabwrite~ -------------------------- */

static t_class *tabwrite_tilde_class;

typedef struct _tabwrite_tilde
{
    t_object x_obj;
    int x_phase;
    int x_nsampsintab;
    t_word *x_vec;
    t_symbol *x_arrayname;
    t_float x_f;
} t_tabwrite_tilde;

static void *tabwrite_tilde_new(t_symbol *s)
{
    t_tabwrite_tilde *x = (t_tabwrite_tilde *)pd_new(tabwrite_tilde_class);
    x->x_phase = 0x7fffffff;
    x->x_arrayname = s;
    x->x_f = 0;
    return (x);
}

static void tabwrite_tilde_redraw(t_tabwrite_tilde *x)
{
    t_garray *a = (t_garray *)pd_findbyclass(x->x_arrayname, garray_class);
    if (!a)
        bug("tabwrite_tilde_redraw");
    else garray_redraw(a);
}

static t_int *tabwrite_tilde_perform(t_int *w)
{
    t_tabwrite_tilde *x = (t_tabwrite_tilde *)(w[1]);
    t_sample *in = (t_sample *)(w[2]);
    int n = (int)(w[3]), phase = x->x_phase, endphase = x->x_nsampsintab;
    if (!x->x_vec) goto bad;

    if (endphase > phase)
    {
        int nxfer = endphase - phase;
        t_word *wp = x->x_vec + phase;
        if (nxfer > n) nxfer = n;
        phase += nxfer;
        while (nxfer--)
        {
            t_sample f = *in++;
            if (PD_BIGORSMALL(f))
                f = 0;
            (wp++)->w_float = f;
        }
        if (phase >= endphase)
        {
            tabwrite_tilde_redraw(x);
            phase = 0x7fffffff;
        }
        x->x_phase = phase;
    }
    else x->x_phase = 0x7fffffff;
bad:
    return (w+4);
}

static void tabwrite_tilde_set(t_tabwrite_tilde *x, t_symbol *s)
{
    t_garray *a;

    x->x_arrayname = s;
    if (!(a = (t_garray *)pd_findbyclass(x->x_arrayname, garray_class)))
    {
        if (*s->s_name) pd_error(x, "tabwrite~: %s: no such array",
            x->x_arrayname->s_name);
        x->x_vec = 0;
    }
    else if (!garray_getfloatwords(a, &x->x_nsampsintab, &x->x_vec))
    {
        pd_error(x, "%s: bad template for tabwrite~", x->x_arrayname->s_name);
        x->x_vec = 0;
    }
    else garray_usedindsp(a);
}

static void tabwrite_tilde_dsp(t_tabwrite_tilde *x, t_signal **sp)
{
    tabwrite_tilde_set(x, x->x_arrayname);
    dsp_add(tabwrite_tilde_perform, 3, x, sp[0]->s_vec, sp[0]->s_n);
}

static void tabwrite_tilde_bang(t_tabwrite_tilde *x)
{
    x->x_phase = 0;
}

static void tabwrite_tilde_start(t_tabwrite_tilde *x, t_floatarg f)
{
    x->x_phase = (f > 0 ? f : 0);
}

static void tabwrite_tilde_stop(t_tabwrite_tilde *x)
{
    if (x->x_phase != 0x7fffffff)
    {
        tabwrite_tilde_redraw(x);
        x->x_phase = 0x7fffffff;
    }
}

static void ntabwrite_tilde_setup(void)
{
    tabwrite_tilde_class = class_new(gensym("ntabwrite~"),
        (t_newmethod)tabwrite_tilde_new, 0,
        sizeof(t_tabwrite_tilde), 0, A_DEFSYM, 0);
    CLASS_MAINSIGNALIN(tabwrite_tilde_class, t_tabwrite_tilde, x_f);
    class_addmethod(tabwrite_tilde_class, (t_method)tabwrite_tilde_dsp,
        gensym("dsp"), A_CANT, 0);
    class_addmethod(tabwrite_tilde_class, (t_method)tabwrite_tilde_set,
        gensym("set"), A_SYMBOL, 0);
    class_addmethod(tabwrite_tilde_class, (t_method)tabwrite_tilde_stop,
        gensym("stop"), 0);
    class_addmethod(tabwrite_tilde_class, (t_method)tabwrite_tilde_start,
        gensym("start"), A_DEFFLOAT, 0);
    class_addbang(tabwrite_tilde_class, tabwrite_tilde_bang);
}

/* ------------ tabplay~ - non-transposing sample playback --------------- */

static t_class *tabplay_tilde_class;

typedef struct _tabplay_tilde
{
    t_object x_obj;
    t_outlet *x_bangout;
    int x_phase;
    int x_nsampsintab;
    int x_start;
    int x_limit;
    t_word *x_vec;
    int x_loop;
    int x_loop_ol;	/* loop overlap size */
    float x_transpose;
    t_symbol *x_arrayname;
    t_clock *x_clock;
    t_tabwrite_tilde *x_tabwriter;
} t_tabplay_tilde;

static void tabplay_tilde_tick(t_tabplay_tilde *x);

static void *tabplay_tilde_new(t_symbol *s)
{
    t_tabplay_tilde *x = (t_tabplay_tilde *)pd_new(tabplay_tilde_class);
    x->x_clock = clock_new(x, (t_method)tabplay_tilde_tick);
    x->x_start = 0;
    x->x_phase = 0x7fffffff;
    x->x_loop = 0;
    x->x_transpose = 1;
    x->x_loop_ol = 0;
    x->x_limit = 0;
    x->x_arrayname = s;
    outlet_new(&x->x_obj, &s_signal);
    x->x_bangout = outlet_new(&x->x_obj, &s_bang);
    return (x);
}

static t_int *tabplay_tilde_perform(t_int *w)
{
    t_tabplay_tilde *x = (t_tabplay_tilde *)(w[1]);
    t_sample *out = (t_sample *)(w[2]);
    t_word *wp;
    int n = (int)(w[3]), phase = x->x_phase,
        endphase = (x->x_nsampsintab < x->x_limit ?
            x->x_nsampsintab : x->x_limit), nxfer, n3;
    int loop_ol; /* loop overlap size */
    int looping; /* the state of looping */
    int fi_idx;  /* fadein index */
    float fadeinfac, fadeoutfac;
    int i;

    t_tabwrite_tilde *tabwriter;

    tabwriter = x->x_tabwriter;
    /*
    if (tabwriter)
    	post ("writephase = %d", tabwriter->x_phase);
    */

    if (!x->x_vec)
    	goto zero;
/*
    if (!x->x_vec || phase >= endphase)
        goto zero;
*/

    if (!x->x_loop) {
        if (phase >= endphase)
		goto zero;
		nxfer = endphase - phase;
		wp = x->x_vec + phase;
		if (nxfer > n)
	    	nxfer = n;
		n3 = n - nxfer;
		phase += nxfer;
		while (nxfer--)
			*out++ = (wp++)->w_float;
		if (phase >= endphase)
		{
	    	clock_delay(x->x_clock, 0);
	    	x->x_phase = 0x7fffffff;
	    	while (n3--)
			*out++ = 0;
		}
		else x->x_phase = phase;
   } else {
   	if (phase >= endphase) {
		outlet_bang(x->x_bangout);
		phase = x->x_start;
	}
   	loop_ol = min((endphase - x->x_start) / 2, x->x_loop_ol);
   	while (n) {
		looping = 0;
		nxfer = endphase - phase - loop_ol;
		if (n < nxfer) {
			nxfer = n;
		} else if (nxfer <= 0) {
			looping = 1;
			nxfer = endphase - phase;
		}

		if (!looping) {
			if (endphase - phase - loop_ol <= 0) /*SDY */
				post ("ntabplay~: internal error 1");
			if (x->x_transpose == 1) {
				for (i = 0; i < nxfer; i++)
					out[i] = (x->x_vec[phase +i]).w_float;
				//memcpy(out, x->x_vec + phase, nxfer * sizeof (t_float));
				phase += nxfer;
			} else {
				if (nxfer != (nxfer / 2) * 2)
					post("ntabplay: transfer count not even (%d)", nxfer);

				for (i = 0, wp = x->x_vec + phase; i < nxfer; i++) {
					out[i] = wp->w_float;
					out[i + 1] = (wp->w_float + (wp +1)->w_float ) /2;
					wp++;
					if (++i > nxfer)
						break;
				}
				phase += nxfer/2;
			}

			
			if (phase >= (endphase - loop_ol) && phase < (endphase - loop_ol + n)) {
				// send a bang out when we start the overlap, which is the start of the loop
				outlet_bang(x->x_bangout);
			}
			if (phase > endphase) /*SDY */
				post ("ntabplay~: internal error 3");
			if (phase >= endphase) {
				phase = x->x_start;
				wp = x->x_vec + phase;
			}
			out += nxfer;
			n -= nxfer;
		} else {
		if (x->x_transpose == 1) {
			for (wp = x->x_vec + phase; nxfer && n; nxfer--, n--) {
			/* SDY for now we only do linear fadein fadeout */
				fadeoutfac = (float)nxfer / (float)(loop_ol);
				fi_idx = loop_ol - (endphase - phase);
				fadeinfac = (float)(fi_idx) / (float)loop_ol;
				*out++ = (wp++)->w_float * fadeoutfac +
					x->x_vec[x->x_start+fi_idx].w_float * fadeinfac;

				if (++phase >= endphase) {
					phase = x->x_start + loop_ol;
					wp = x->x_vec + phase;
// SDY this should not happen but it has at some point
//						if (nxfer != 1)
//						post ("ntabplay~: intr err 4");
				}
			}
		} else {
			for (wp = x->x_vec + phase; nxfer && n; nxfer--, n--) {
			/* SDY for now we only do linear fadein fadeout */
				fadeoutfac = (float)nxfer / (float)(loop_ol);
				fi_idx = loop_ol - (endphase - phase);
				fadeinfac = (float)(fi_idx) / (float)loop_ol;
				*out++ = wp->w_float * fadeoutfac +
					x->x_vec[x->x_start+fi_idx].w_float * fadeinfac;


				if (++phase >= endphase) {
					float f;

					outlet_bang(x->x_bangout);
					f = wp->w_float;
					phase = x->x_start + loop_ol;
					wp = x->x_vec + phase;
					*out++ = (wp->w_float+f)/2 * fadeoutfac +
					(x->x_vec[x->x_start+fi_idx].w_float  +
					 	x->x_vec[x->x_start+fi_idx + 1].w_float)/ 2 * fadeinfac;
// SDY This should not happen but it has at some point
//					if (nxfer != 1)
//						post ("ntabplay~: intr err 4");
				} else {
					*out++ = (wp->w_float + (wp+1)->w_float)/2 * fadeoutfac +
					(x->x_vec[x->x_start+fi_idx].w_float  +
					 	x->x_vec[x->x_start+fi_idx + 1].w_float)/ 2 * fadeinfac;
					wp++;
				}
					
				if (!--nxfer)
					break;
			}

	}

		}
	}
        x->x_phase = phase;
    }

    return (w+4);
zero:
    while (n--) *out++ = 0;
    return (w+4);
}

static void tabplay_tilde_set(t_tabplay_tilde *x, t_symbol *s)
{
    t_garray *a;

    x->x_arrayname = s;
    if (!(a = (t_garray *)pd_findbyclass(x->x_arrayname, garray_class)))
    {
        if (*s->s_name) pd_error(x, "ntabplay~: %s: no such array",
            x->x_arrayname->s_name);
        x->x_vec = 0;
    }
    else if (!garray_getfloatwords(a, &x->x_nsampsintab, &x->x_vec))
    {
        pd_error(x, "%s: bad template for ntabplay~", x->x_arrayname->s_name);
        x->x_vec = 0;
    }
    else garray_usedindsp(a);
}

static void tabplay_tilde_dsp(t_tabplay_tilde *x, t_signal **sp)
{
    t_tabwrite_tilde *tabwriter;

    tabwriter = (t_tabwrite_tilde *)pd_findbyclass(x->x_arrayname,
    							tabwrite_tilde_class);
/* SDY new version?
    if (tabwriter)
    	post("found");
    else
    	post ("notfound");
*/
    x->x_tabwriter = tabwriter;

    tabplay_tilde_set(x, x->x_arrayname);
    dsp_add(tabplay_tilde_perform, 3, x, sp[0]->s_vec, sp[0]->s_n);
}

static void tabplay_tilde_list(t_tabplay_tilde *x, t_symbol *s,
    int argc, t_atom *argv)
{
    long start = atom_getfloatarg(0, argc, argv);
    long length = atom_getfloatarg(1, argc, argv);
    if (start < 0) start = 0;
    if (length <= 0)
        x->x_limit = 0x7fffffff;
    else
    	x->x_limit = start + length;
    x->x_phase = start;
    x->x_start = start;
}
static void
tabplay_tilde_trans(t_tabplay_tilde *x, t_float tr)
{
	x->x_transpose = !x->x_transpose;

	post ("ntabplay: transpose = %f\n", x->x_transpose);
}
		

static void
tabplay_tilde_loop(t_tabplay_tilde *x, t_float start, t_float length,t_float ol)
{
    x->x_loop = 1;
    start = (long) start;
    length = (long) length;

    if (start < 0) start = 0;
    if (length <= 0)
    	x->x_limit = 0x7fffffff;
    else
    	x->x_limit = start + length;
    x->x_start = start;
    x->x_loop_ol = (long) ol;
}

static void tabplay_tilde_stop(t_tabplay_tilde *x)
{
    x->x_phase = 0x7fffffff;
    x->x_loop = 0;
    x->x_loop_ol = 0;
}

static void tabplay_tilde_tick(t_tabplay_tilde *x)
{
    outlet_bang(x->x_bangout);
}

static void tabplay_tilde_free(t_tabplay_tilde *x)
{
    clock_free(x->x_clock);
}

void ntabplay_tilde_setup(void)
{
    tabplay_tilde_class = class_new(gensym("ntabplay~"),
        (t_newmethod)tabplay_tilde_new, (t_method)tabplay_tilde_free,
        sizeof(t_tabplay_tilde), 0, A_DEFSYM, 0);
    class_addmethod(tabplay_tilde_class, (t_method)tabplay_tilde_dsp,
        gensym("dsp"), A_CANT, 0);
    class_addmethod(tabplay_tilde_class, (t_method)tabplay_tilde_stop,
    	gensym("stop"), 0);
    class_addmethod(tabplay_tilde_class, (t_method)tabplay_tilde_loop,
    	gensym("loop"), A_FLOAT, A_FLOAT, A_FLOAT, 0);
    class_addmethod(tabplay_tilde_class, (t_method)tabplay_tilde_trans,
    	gensym("transpose"), A_FLOAT, 0);
    class_addmethod(tabplay_tilde_class, (t_method)tabplay_tilde_set,
        gensym("set"), A_DEFSYM, 0);
    class_addlist(tabplay_tilde_class, tabplay_tilde_list);

    ntabwrite_tilde_setup();
}
